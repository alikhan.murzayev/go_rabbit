package main

import (
	"encoding/json"
	"fmt"
	"github.com/djumanoff/amqp"
)

var cfg = amqp.Config{
	Host:     "localhost",
	Port:     5672,
	User:     "guest",
	Password: "guest",
	LogLevel: 5,
}

var cltCfg = amqp.ClientConfig{
	//RequestX:  "",
	//ResponseX: "",
	//ResponseQ: "",
}

func main() {
	fmt.Println("Start")

	sess := amqp.NewSession(cfg)
	if err := sess.Connect(); err != nil {
		fmt.Println(err)
		return
	}

	clt, err := sess.Client(cltCfg)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {

		var req HelloSrvReq
		var resp HelloSrvResp

		fmt.Print("What's your name?\n--> ")
		_, err := fmt.Scan(&req.Name)
		if err != nil {
			fmt.Println(err)
			return
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			fmt.Println(err)
			return
		}

		reply, err := clt.Call("request.get.hello", amqp.Message{Body: reqBytes})
		if err != nil {
			fmt.Println(err)
			return
		}

		if err := json.Unmarshal(reply.Body, &resp); err != nil {
			fmt.Println(err)
			return
		}

		fmt.Printf("--> %s\n", resp.Msg)
		break
	}

	fmt.Println("End")

}

type HelloSrvReq struct {
	Name string `json:"name"`
}

type HelloSrvResp struct {
	Msg string `json:"msg"`
}
