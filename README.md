# RabbitMQ + [djumanoff/amqp](https://github.com/djumanoff/amqp) + Go simple example.
## Usage:
#### 1. Clone repository:
```shell script
git clone https://gitlab.com/alikhan.murzayev/go_rabbit
cd go_rabbit
```
#### 2. Run RabbitMQ in docker:
```shell script
docker run -d --name rabbitmq -p 5672:5672 rabbitmq
```
#### 3. Run server:
```shell script
go run server/server.go 
```
#### 4. Run client:
```shell script
go run client/client.go
```
