package main

import (
	"encoding/json"
	"fmt"
	"github.com/djumanoff/amqp"
)

var cfg = amqp.Config{
	Host:     "localhost",
	Port:     5672,
	User:     "guest",
	Password: "guest",
	LogLevel: 5,
}

var srvCfg = amqp.ServerConfig{
	//RequestX:  "",
	//ResponseX: "",
}

func main() {
	fmt.Println("Start")

	sess := amqp.NewSession(cfg)

	if err := sess.Connect(); err != nil {
		fmt.Println(err)
		return
	}
	defer sess.Close()

	srv, err := sess.Server(srvCfg)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = srv.Endpoint("request.get.hello", handler)
	if err != nil {
		fmt.Println(err)
		return
	}

	if err := srv.Start(); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("End")

}

type HelloSrvReq struct {
	Name string `json:"name"`
}

type HelloSrvResp struct {
	Msg string `json:"msg"`
}

func handler(m amqp.Message) *amqp.Message {

	var req HelloSrvReq
	var resp HelloSrvResp

	if err := json.Unmarshal(m.Body, &req); err != nil {
		resp.Msg = fmt.Sprintf("unable to unmarshall: %v", err.Error())
		respBytes, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}

		return &amqp.Message{Body: respBytes}
	}

	resp.Msg = fmt.Sprintf("Hello, %s", req.Name)
	respBytes, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}

	return &amqp.Message{Body: respBytes}

}
